# Ce script utilise FoldX pour analyser les interfaces protéine-protéine présentes dans le répertoire courant.
import os
import subprocess

# Spécifier le chemin complet vers l'exécutable FoldX
foldx_executable = "/home/E23B371C/Bureau/foldxLinux64_0/foldx_20241231"

# Récupérer le répertoire courant
current_directory = os.getcwd()

# Liste tous les fichiers '_0001.pdb' dans le répertoire courant
pdb_files = [file for file in os.listdir(current_directory) if file.endswith("_0001.pdb")]

# Boucle sur chaque fichier pdb trouvé
for pdb_file in pdb_files:
    # Construction de la commande FoldX
    command = [
        foldx_executable,
        "--command=AnalyseComplex",
        f"--pdb={pdb_file}",
        "--analyseComplexChains=A,B"
    ]

    # Exécution de la commande FoldX
    subprocess.run(command)

    print(f"FoldX Analysis completed for {pdb_file}")

print("All FoldX Analyses completed.")

# Supprimer les fichiers .fxout qui ne commencent pas par 'Interface_Residues'
for file in os.listdir(current_directory):
    if file.endswith(".fxout") and not file.startswith("Interface_Residues"):
        os.remove(os.path.join(current_directory, file))

print("Non-Interface_Residues .fxout files deleted.")
