# Ce script traite les fichiers PDB en ajoutant les lignes CRYST1 manquantes, en renumérotant les lettres de chaîne, et en supprimant les séquences d'ADN.
import os

def renumber_chain_letters(input_file_path):
    try:
        with open(input_file_path, 'r') as file:
            lines = file.readlines()

        new_lines = []
        current_chain_letter = None
        alphabet_counter = ord('A')
        for line in lines:
            if line.startswith('ATOM') or line.startswith('HETATM'):
                # Extraire la partie avant et après la cinquième colonne
                line_before_col_5 = line[:21]
                chain_letter = line[21:22]
                line_after_col_5 = line[22:]

                if chain_letter.isalpha():  # Vérifier si la lettre est une lettre
                    if chain_letter != current_chain_letter:
                        current_chain_letter = chain_letter
                        next_chain_letter = chr(alphabet_counter)
                        alphabet_counter += 1  # Passer à la lettre suivante dans l'ordre alphabétique
                    new_chain_letter = next_chain_letter
                    # Recréer la ligne avec la nouvelle lettre tout en préservant la mise en forme
                    new_line = f"{line_before_col_5}{new_chain_letter}{line_after_col_5}"
                    new_lines.append(new_line)
                else:
                    new_lines.append(line)
            else:
                new_lines.append(line)

        with open(input_file_path, 'w') as file:
            file.writelines(new_lines)
    except IOError as e:
        print(f"Une erreur est survenue : {e}")

def remove_dna(input_pdb_file):
    asp_lines = []
    with open(input_pdb_file, 'r') as f:
        for line in f:
            if line.startswith('CRYST1') or (line.startswith('ATOM') and line[17:20].strip() in ["ALA", "ARG", "ASN", "ASP", "CYS", "GLN", "GLU", "GLY", "HIS", "ILE", "LEU", "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL"]):
                asp_lines.append(line)

    # Écrivez les lignes filtrées dans le même fichier d'entrée
    with open(input_pdb_file, 'w') as f:
        f.write(''.join(asp_lines))

# Script bash intégré
bash_script = """
#!/bin/bash
# Ce script bash récupère la ligne commencant par CRYST1 du fichier PDB brut et la copie dans le fichier issu de ROSETTA RELAX pour le rendre exécutable pour 
# une DSSP

# Récupérer tous les fichiers .pdb dans le répertoire courant
for pdb_file in *.pdb; do
    # Extraire la ligne CRYST1 du fichier pdb
    cryst1_line=$(grep "^CRYST1" "$pdb_file")
    
    # Si la ligne CRYST1 est trouvée, copier son contenu dans la ligne juste avant la première occurence du mot "ATOM"
    if [ -n "$cryst1_line" ]; then
        # Vérifier si la ligne CRYST1 est déjà présente dans le fichier issu de Rosetta Relax
        if ! grep -q "^CRYST1" "${pdb_file%.*}_0001.pdb"; then
            # Trouver la ligne juste avant la première occurence du mot "ATOM"
            line_before_atom=$(awk '/^ATOM/{print NR; exit}' "${pdb_file%.*}_0001.pdb")
            
            if [ -n "$line_before_atom" ]; then
                # Copier le contenu du fichier _0001.pdb temporairement dans un fichier temporaire
                cp "${pdb_file%.*}_0001.pdb" tmp_0001.pdb
                
                # Insérer la ligne CRYST1 juste avant la première occurence du mot "ATOM"
                awk -v line="$line_before_atom" -v line_to_insert="$cryst1_line" 'NR == line {print line_to_insert} {print}' tmp_0001.pdb > "${pdb_file%.*}_0001.pdb"
            fi
        fi
    fi
done
"""

if __name__ == "__main__":
    # Exécuter le script bash intégré
    os.system(bash_script)

    # Récupère le chemin du répertoire courant
    current_directory = os.getcwd()

    # Liste tous les fichiers dans le répertoire courant
    for file_name in os.listdir(current_directory):
        if file_name.endswith('_0001.pdb'):
            file_path = os.path.join(current_directory, file_name)
            renumber_chain_letters(file_path)

    # Récupérer tous les fichiers _0001.pdb dans le répertoire courant
    pdb_files = [file for file in os.listdir() if file.endswith("_0001.pdb")]

    # Parcourir tous les fichiers et les filtrer
    for pdb_file in pdb_files:
        remove_dna(pdb_file)
        print("Les modifications ont été apportées avec succès au fichier", pdb_file)
