# Ce script lit les résultats d'un fichier donné, calcule les fréquences des acides aminés par identifiant PDB, et génère un graphique de ces fréquences,
# ainsi qu'un fichier CSV des données traitées.

import os
import sys
import csv
import matplotlib.pyplot as plt
from collections import Counter

def lire_resultats(filename):
    """
    Fonction pour lire le fichier de résultats et retourner les données sous forme de liste de tuples.
    Chaque tuple contient l'identifiant PDB, la chaîne, l'acide aminé et le compteur.
    """
    data = []
    with open(filename, 'r') as file:
        for line in file:
            parts = line.strip().split(', ')
            pdb_id = parts[0].split(': ')[1]
            chain = parts[1].split(': ')[1]
            amino_acid = parts[3].split(': ')[1]
            data.append((pdb_id, chain, amino_acid))
    
    return data

def generer_graphique_effectif_acides_amines(data, fichier_sortie):
    """
    Fonction pour générer un graphique montrant l'effectif de tous les acides aminés.
    """
    # Compter les acides aminés
    acides_count = Counter([entry[2] for entry in data])

    # Trier les acides aminés par effectif (du plus grand au plus petit)
    acides_count_sorted = dict(sorted(acides_count.items(), key=lambda item: item[1], reverse=True))

    plt.figure(figsize=(12, 6))
    
    # Récupérer les acides aminés triés et leurs effectifs
    acides = list(acides_count_sorted.keys())
    counts = list(acides_count_sorted.values())
    
    plt.bar(acides, counts)
    plt.xlabel('Acides aminés')
    plt.ylabel('Fréquences')
    plt.title(f'Fréquences des résidus d\'interface PP formant un feuillet beta')
    plt.xticks(rotation=45)
    plt.tight_layout()
    
    # Définir les étiquettes de l'axe y comme des nombres entiers
    plt.yticks(range(max(counts) + 1))
    
    # Enregistrer le graphique dans un fichier
    plt.savefig(fichier_sortie)
    
    # Afficher le nom du fichier sauvegardé
    print(f"Graphique enregistré sous: {fichier_sortie}")

def sauvegarder_csv(data, fichier_sortie):
    """
    Fonction pour sauvegarder les données dans un fichier CSV.
    """
    with open(fichier_sortie, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['PDB_ID', 'Chain', 'Acide Aminé'])
        writer.writerows(data)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py fichier_de_resultats")
        sys.exit(1)
    
    fichier_entree = sys.argv[1]
    
    if not os.path.exists(fichier_entree):
        print(f"Le fichier {fichier_entree} n'existe pas.")
        sys.exit(1)
        
    data = lire_resultats(fichier_entree)
    nom_fichier_sortie_graphique = f"{os.path.splitext(fichier_entree)[0]}_graphique.png"
    generer_graphique_effectif_acides_amines(data, nom_fichier_sortie_graphique)
    
    nom_fichier_sortie_csv = f"{os.path.splitext(fichier_entree)[0]}_donnees.csv"
    sauvegarder_csv(data, nom_fichier_sortie_csv)
    print(f"Données sauvegardées dans le fichier CSV: {nom_fichier_sortie_csv}")
