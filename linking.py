# Ce script traite les fichiers DSSP et FoldX pour trouver des correspondances entre eux et sauvegarde les résultats dans un fichier de sortie.
import os
import re
import sys

def lire_fichier_dssp(filename):
    """
    Fonction pour lire un fichier DSSP et retourner les données sous forme de dictionnaire.
    """
    results = {}
    with open(filename, 'r') as file:
        current_pdb_id = None
        for line in file:
            if line.startswith('Résultats pour'):
                current_pdb_id = line.split()[-1][:-1]  # Extrait l'identifiant PDB
                results[current_pdb_id] = []
            else:
                fields = re.split(r'\s+', line.strip())  # Utilisation de l'expression régulière pour diviser en fonction de l'espace
                if len(fields) == 4:
                    chain, position, amino_acid, _ = fields
                    results[current_pdb_id].append((chain, int(position), amino_acid))
    return results

def lire_fichier_foldx(filename):
    """
    Fonction pour lire un fichier FoldX et retourner les données sous forme de dictionnaire.
    """
    results = {}
    with open(filename, 'r') as file:
        for line in file:
            if line.startswith('Interface residues in beta for'):
                pdb_id = line.split()[-1][2:-4]  # Supprimer le préfixe et l'extension pour extraire l'identifiant PDB
                results[pdb_id] = []
            else:
                parts = line.split()
                if len(parts) == 3:  # Vérifie si la ligne peut être correctement divisée en trois parties
                    try:
                        chain, position, amino_acid = parts
                        position = int(position)
                        results[pdb_id].append((chain, position, amino_acid))
                    except ValueError:
                        pass  # Ignorer les lignes qui ne peuvent pas être traitées correctement
    return results


def trouver_correspondances(fichier_dssp, fichier_sortie):
    dictionnaire_dssp = lire_fichier_dssp(fichier_dssp)
    print("Données extraites du fichier DSSP:")
    print(dictionnaire_dssp)

    correspondances_globales = []

    # Parcours de tous les fichiers .fxout dans le répertoire courant
    repertoire_courant = os.getcwd()
    for fichier in os.listdir(repertoire_courant):
        if fichier.endswith(".fxout"):
            chemin_fichier_foldx = os.path.join(repertoire_courant, fichier)
            dictionnaire_foldx = lire_fichier_foldx(chemin_fichier_foldx)
            print("Données extraites du fichier FoldX:", fichier)
            print(dictionnaire_foldx)

            correspondances = []

            for pdb_id_dssp, positions_dssp in dictionnaire_dssp.items():
                if pdb_id_dssp in dictionnaire_foldx:  # Vérifier si l'identifiant PDB existe dans le dictionnaire FoldX
                    positions_foldx = dictionnaire_foldx[pdb_id_dssp]
                    for triplet_dssp in positions_dssp:
                        for triplet_foldx in positions_foldx:
                            # Comparer les triplets seulement si les identifiants PDB correspondent
                            if triplet_dssp[0] == triplet_foldx[0] and triplet_dssp[1] == triplet_foldx[1]:
                                correspondances.append((pdb_id_dssp, triplet_dssp))

            correspondances_globales.extend(correspondances)

    # Sauvegarde des résultats dans le fichier de sortie
    with open(fichier_sortie, 'a') as output_file:
        for pdb_id, triplet in correspondances_globales:
            chain, position, amino_acid = triplet
            output_file.write(f"Identifiant PDB: {pdb_id}, Chaîne: {chain}, Position: {position}, Acide aminé: {amino_acid}\n")

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python script.py fichier_dssp fichier_sortie")
        sys.exit(1)

    fichier_dssp = sys.argv[1]
    fichier_sortie = sys.argv[2]

    trouver_correspondances(fichier_dssp, fichier_sortie)
    print("\nLes résultats ont été ajoutés au fichier", fichier_sortie)









