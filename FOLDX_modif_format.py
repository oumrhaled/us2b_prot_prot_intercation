# Ce script lit les fichiers de sortie de FoldX (.fxout), extrait les informations sur les résidus d'interface, met à jour les fichiers avec 
#ces informations et nettoie les lignes indésirables.

import os

# Fonction pour lire les données à partir du fichier FoldX
def lire_foldx(fichier_foldx):
    interface_residues = {}
    pdb_file = None  # Initialisation de pdb_file à None
    with open(fichier_foldx, 'r') as f:
        lines = f.readlines()
        for line in lines:
            if "List of Residues at the interface" in line:
                pdb_file = line.split(":")[1].strip()
                interface_residues[pdb_file] = []
            elif pdb_file is not None and line.strip() and not line.startswith("FoldX"):
                residues = line.split()
                for res in residues:
                    if len(res) >= 3:  # Vérifier si la chaîne est valide
                        chain = res[1]
                        position = res[2:]
                        amino_acid = res[0]
                        interface_residues[pdb_file].append((chain, position, amino_acid))
    return interface_residues

# Fonction pour mettre à jour les fichiers .fxout
def mettre_a_jour_fichiers(fichier_foldx, interface_residues_foldx):
    with open(fichier_foldx, 'r+') as f:
        lines = f.readlines()
        f.seek(0)
        for line in lines:
            if "List of Residues at the interface" in line:
                pdb_file = line.split(":")[1].strip()
                f.write("Interface residues in beta for: {}\n".format(pdb_file))
                f.write("Residues from FoldX:\n")
                for residue in interface_residues_foldx.get(pdb_file, []):
                    f.write("{} {} {}\n".format(residue[0], residue[1], residue[2]))
            else:
                f.write(line)
        f.truncate()
        # Ajouter le nom du fichier au fichier de suivi
        with open("fichiers_modifies.txt", "a") as tracking_file:
            tracking_file.write(fichier_foldx + "\n")

# Fonction pour nettoyer les fichiers .fxout
def nettoyer_fichier(fichier):
    with open(fichier, 'r+') as f:
        lines = f.readlines()
        f.seek(0)
        for i, line in enumerate(lines):
            # Supprimer les parties indésirables (les cinq premières lignes et la dernière ligne)
            if i < 6 or i == len(lines) - 1:
                continue
            if not ("FoldX" in line or "interface residues between A and B" in line):
                f.write(line.rstrip() + '\n')  # Ajouter un saut de ligne après chaque écriture
        f.truncate()

# Parcours de tous les fichiers .fxout dans le répertoire courant
repertoire_courant = os.getcwd()
fichiers_modifies = set()
if os.path.exists("fichiers_modifies.txt"):
    with open("fichiers_modifies.txt", "r") as tracking_file:
        fichiers_modifies = set(tracking_file.read().splitlines())

for fichier in os.listdir(repertoire_courant):
    if fichier.endswith(".fxout") and fichier not in fichiers_modifies:
        chemin_fichier = os.path.join(repertoire_courant, fichier)
        # Utilisation de la fonction pour extraire et mettre à jour les informations
        interface_residues_foldx = lire_foldx(chemin_fichier)
        mettre_a_jour_fichiers(chemin_fichier, interface_residues_foldx)
        # Utilisation de la fonction pour nettoyer le fichier
        nettoyer_fichier(chemin_fichier)
        print("Les informations ont été mises à jour dans", chemin_fichier)





# import os

# # Fonction pour lire les données à partir du fichier FoldX
# def lire_foldx(fichier_foldx):
#     interface_residues = {}
#     pdb_file = None  # Initialisation de pdb_file à None
#     with open(fichier_foldx, 'r') as f:
#         lines = f.readlines()
#         for line in lines:
#             if "List of Residues at the interface" in line:
#                 pdb_file = line.split(":")[1].strip()
#                 interface_residues[pdb_file] = []
#             elif pdb_file is not None and line.strip() and not line.startswith("FoldX"):
#                 residues = line.split()
#                 for res in residues:
#                     if len(res) >= 3:  # Vérifier si la chaîne est valide
#                         chain = res[1]
#                         position = res[2:]
#                         amino_acid = res[0]
#                         interface_residues[pdb_file].append((chain, position, amino_acid))
#     return interface_residues

# # Fonction pour mettre à jour les fichiers .fxout
# def mettre_a_jour_fichiers(fichier_foldx, interface_residues_foldx):
#     with open(fichier_foldx, 'r+') as f:
#         lines = f.readlines()
#         f.seek(0)
#         for line in lines:
#             if "List of Residues at the interface" in line:
#                 pdb_file = line.split(":")[1].strip()
#                 f.write("Interface residues in beta for: {}\n".format(pdb_file))
#                 f.write("Residues from FoldX:\n")
#                 for residue in interface_residues_foldx.get(pdb_file, []):
#                     f.write("{} {} {}\n".format(residue[0], residue[1], residue[2]))
#             else:
#                 f.write(line)
#         f.truncate()

# # Fonction pour nettoyer les fichiers .fxout
# def nettoyer_fichier(fichier):
#     with open(fichier, 'r+') as f:
#         lines = f.readlines()
#         f.seek(0)
#         for i, line in enumerate(lines):
#             # Supprimer les parties indésirables (les cinq premières lignes et la dernière ligne)
#             if i < 6 or i == len(lines) - 1:
#                 continue
#             if not ("FoldX" in line or "interface residues between A and B" in line):
#                 f.write(line.rstrip() + '\n')  # Ajouter un saut de ligne après chaque écriture
#         f.truncate()

# # Parcours de tous les fichiers .fxout dans le répertoire courant
# repertoire_courant = os.getcwd()
# for fichier in os.listdir(repertoire_courant):
#     if fichier.endswith(".fxout"):
#         chemin_fichier = os.path.join(repertoire_courant, fichier)
#         # Utilisation de la fonction pour extraire et mettre à jour les informations
#         interface_residues_foldx = lire_foldx(chemin_fichier)
#         mettre_a_jour_fichiers(chemin_fichier, interface_residues_foldx)
#         # Utilisation de la fonction pour nettoyer le fichier
#         nettoyer_fichier(chemin_fichier)
#         print("Les informations ont été mises à jour dans", chemin_fichier)









