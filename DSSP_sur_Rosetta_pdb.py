# Ce script lit les fichiers PDB dans le répertoire courant, calcule les valeurs DSSP pour chaque fichier,
# et écrit les résultats dans un fichier de sortie.

import os
from Bio.PDB import PDBParser
from Bio.PDB.DSSP import DSSP
import sys

def calculate_dssp_for_files(directory, output_file):
    """
    Calcule les valeurs DSSP pour les fichiers PDB dans un répertoire donné.

    Args:
        directory (str): Le chemin du répertoire contenant les fichiers PDB.
        output_file (str): Le chemin du fichier de sortie pour écrire les résultats DSSP.
    """
    with open(output_file, 'w') as f:
        pdb_files = [f for f in os.listdir(directory) if f.endswith("_0001.pdb")]
        
    
        for pdb_file in pdb_files:
            pdb_id = os.path.splitext(pdb_file)[0]
            pdb_file_path = os.path.join(directory, pdb_file)
            parser = PDBParser()
            structure = parser.get_structure(pdb_id, pdb_file_path)
            model = structure[0]
            dssp = DSSP(model, pdb_file_path, dssp='mkdssp')
            
            f.write(f"Résultats pour {pdb_id}:\n")
            for i in list(dssp.keys()):
                # Extraire les colonnes nécessaires
                chain = i[0]  # Première colonne
                aa_resnum = i[1][1]  # Colonne contenant 55
                residue = dssp[i][1]
                ss = dssp[i][2]
                
                # Écrire dans le fichier avec un format spécifique
                f.write(f"{chain:1}    {aa_resnum:3}    {residue:2}    {ss}\n")
            f.write("\n")

def main(output_file):
    """
    Fonction principale pour générer les résultats DSSP pour les fichiers PDB.

    Args:
        output_file (str): Le chemin du fichier de sortie pour écrire les résultats DSSP.
    """
    current_directory = os.getcwd()
    calculate_dssp_for_files(current_directory, output_file)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py nom_fichier_resultats")
        sys.exit(1)
    nom_fichier_resultats = sys.argv[1] 
    main(nom_fichier_resultats)




