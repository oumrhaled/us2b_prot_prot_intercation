# Ce script lit les résultats d'un fichier donné, calcule les fréquences des acides aminés par identifiant PDB, et génère des graphiques de ces fréquences.
import os
import sys
import matplotlib.pyplot as plt

def lire_resultats(filename):
    """
    Fonction pour lire le fichier de résultats et retourner les données sous forme de dictionnaire.
    """
    results = {}
    with open(filename, 'r') as file:
        for line in file:
            parts = line.strip().split(', ')
            pdb_id = parts[0].split(': ')[1]
            amino_acid = parts[3].split(': ')[1]
            if pdb_id not in results:
                results[pdb_id] = {}
            if amino_acid not in results[pdb_id]:
                results[pdb_id][amino_acid] = 1
            else:
                results[pdb_id][amino_acid] += 1
    return results

def generer_graphiques(resultats, fichier_sortie):
    """
    Fonction pour générer des graphiques de fréquence d'acides aminés pour chaque identifiant PDB.
    """
    for pdb_id, frequencies in resultats.items():
        # Tri des fréquences du plus grand au plus petit
        frequencies_sorted = dict(sorted(frequencies.items(), key=lambda item: item[1], reverse=True))
        
        labels = frequencies_sorted.keys()
        counts = frequencies_sorted.values()

        plt.figure(figsize=(10, 6))
        plt.bar(labels, counts)
        plt.xlabel('Acides aminés')
        plt.ylabel('Fréquences')
        plt.title(f'Fréquences des résidus d\'interface PP formant un feuillet beta pour l\'identifiant PDB {pdb_id}')
        plt.xticks(rotation=45)
        
        # Définir les étiquettes de l'axe y comme des nombres entiers
        plt.yticks(range(max(counts) + 1))
        
        plt.tight_layout()
        
        # Enregistrer le graphique dans un fichier
        nom_fichier = f"{pdb_id}_{fichier_sortie}"
        plt.savefig(nom_fichier)
        
        # Afficher le nom du fichier sauvegardé
        print(f"Graphique enregistré sous: {nom_fichier}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python script.py nom_fichier_entree")
        sys.exit(1)
    
    fichier_entree = sys.argv[1]
    resultats = lire_resultats(fichier_entree)
    generer_graphiques(resultats, "graphique.png")
