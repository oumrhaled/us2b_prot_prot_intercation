# Ce script utilise Rosetta pour effectuer une relaxation des structures PDB présentes dans le répertoire courant en parallèle.
import subprocess
import os
from multiprocessing import Pool

def rosetta_relax(pdb_file):
    rosetta_path = "/opt/site/ubuntu_22.04/rosetta/2023.45/main/source/bin/relax.linuxgccrelease"
    database_path = "/opt/site/ubuntu_22.04/rosetta/2023.45/main/database/"
    
    pdb_id = os.path.splitext(pdb_file)[0]  # Extract PDB ID from file name
    
    command = [
        rosetta_path,
        "-database", database_path,
        "-s", pdb_file,
        "-nstruct", "1",
        "-in:file:fullatom"
    ]
    
    try:
        subprocess.run(command, check=True)
        print(f"Relaxation with Rosetta completed successfully for {pdb_id}.")
    except subprocess.CalledProcessError as e:
        print(f"Error: Rosetta relaxation failed for {pdb_id} with exit code {e.returncode}")

def main():
    # Obtenir la liste des fichiers PDB dans le répertoire actuel
    pdb_files = [file for file in os.listdir() if file.endswith(".pdb")]

    # Exécuter les relaxations avec Rosetta en parallèle
    with Pool(processes=8) as pool:
        pool.map(rosetta_relax, pdb_files)

if __name__ == "__main__":
    main()
