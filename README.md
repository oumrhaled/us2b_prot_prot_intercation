# Projet Stage

# Projet d'Analyse Structurale des Protéines

Ce projet vise à automatiser l'analyse structurale des protéines en utilisant plusieurs outils bioinformatiques, notamment Rosetta, DSSP et FoldX, et en utilisant Python pour orchestrer le flux de travail.

## Objectif

L'objectif principal de ce projet est d'analyser les structures de protéines pour identifier les résidus qui se trouvent à l'interface et qui possèdent une structure secondaire de feuillet bêta. Cette analyse permet de comprendre les interactions protéine-protéine et d'identifier les régions clés pour la fonction ou la liaison moléculaire.

## Fonctionnalités

- Lancement automatique de la relaxation de structures protéiques avec Rosetta.
- Nettoyage et préparation des fichiers PDB relaxés pour l'analyse.
- Identification des feuilles bêta à l'aide de DSSP.
- Utilisation de FoldX pour récupérer les résidus d'interface.
- Croisement des informations de FoldX et DSSP pour créer des représentations graphiques sous forme d'histogrammes.

## Configuration requise

- Python 3.x
- Rosetta
- DSSP
- FoldX

## Installation

1. Cloner ce dépôt :

```bash
git clone https://gitlab.com/oumrhaled/us2b_prot_prot_intercation.git
```


2. Naviguer vers le Dossier PROTOTYPE_CODE

=======
## Utilisation

## Utilisation du script de téléchargement batch_download.sh

Pour télécharger les fichiers PDB nécessaires à partir de RCSB Protein Data Bank, suivez ces étapes :

1. Assurez-vous d'avoir le script `batch_download.sh` dans votre répertoire de travail.

2. Créez un fichier texte `liste_PDB.txt` contenant les identifiants PDB des structures que vous souhaitez télécharger, séparés par des virgules (par exemple, `1abc,2xyz,3def`).

3. Exécutez le script `batch_download.sh` en spécifiant les options appropriées selon vos besoins :

```bash
./batch_download.sh -f liste_PDB.txt -o cheminer_vers_votre_dossier_de_sortie -p
```
## Lancement de ROSETTA RELAX
## Configuration des chemins vers Rosetta

Avant d'exécuter le script `auto_ROSETTA_parallel11.py`, assurez-vous d'avoir configuré les chemins vers les exécutables de Rosetta et sa base de données. Voici comment faire :

1. Ouvrez le script Python `auto_ROSETTA_parallel11.py` dans un éditeur de texte.

2. Recherchez les variables `rosetta_path` et `database_path` dans le script. Elles sont utilisées pour spécifier les chemins vers les exécutables de Rosetta et sa base de données, respectivement.

3. Modifiez ces variables pour qu'elles pointent vers les emplacements corrects sur votre système. Par exemple :

```python
rosetta_path = "/chemin/vers/votre/rosetta/main/source/bin/relax.linuxgccrelease"
database_path = "/chemin/vers/votre/rosetta/main/database/"
```
Exécuter le code :
```bash
python auto_ROSETTA_parallel11.py 
```
( vous pouvez pouvez utiliser `nohup`) 

## Nettoyage et préparation des fichiers pour FOLDX et DSSP

Après avoir généré les fichiers avec l'extension `_0001.pdb` à partir de Rosetta Relax, vous devez nettoyer et préparer ces fichiers pour l'analyse ultérieure avec FOLDX et DSSP. Voici comment faire :

1. Assurez-vous que les fichiers `_0001.pdb` générés par Rosetta Relax sont présents dans le même répertoire que le script `preparation_pdb.py`.

2. Exécutez le script `preparation_pdb.py` en utilisant Python :

```bash
python preparation_pdb.py
```
Ce script effectue les opérations suivantes :

    Renomme les lettres de chaîne dans les fichiers PDB pour assurer la continuité de la numérotation des chaînes.
    Supprime les résidus d'ADN des fichiers PDB, car FOLDX et DSSP ne nécessitent que des résidus d'acides aminés.

    Attendez que le script se termine. Les modifications seront apportées aux fichiers _0001.pdb dans le répertoire courant.


## Lancement de DSSP sur les structures sélectionnées

Une fois que les fichiers ont été nettoyés et préparés avec succès, vous pouvez utiliser le script `DSSP_sur_structure_selectionnee.py` pour exécuter DSSP et obtenir des informations sur les structures secondaires des résidus.

### Exécution du script

Pour exécuter ce script, utilisez la commande suivante :

```bash
python DSSP_sur_structure_selectionnee.py nom_fichier_resultats structure_secondaire
```

- `nom_fichier_resultats` : le nom du fichier de sortie pour écrire les résultats DSSP.
- `structure_secondaire` : le type de structure secondaire à sélectionner ('H' pour hélice, 'E' pour feuillet β, etc.).

Par exemple, pour exécuter DSSP et enregistrer les résultats dans un fichier nommé `resultats_dssp.txt` en ne sélectionnant que les feuilles β, vous pouvez utiliser la commande suivante :

```bash
python DSSP_sur_structure_selectionnee.py resultats_dssp.txt E
```
si vou souhaitez générer DSSP en ne séléctionnate aucune struture , exécutez le script de `DSSP_sur_Rosetta_pdb.py`: Ce script génère une prédiction de structure secondaire génrale en affichant toutes les strutures possibles.
### Remarque

Assurez-vous que les fichiers `_0001.pdb` générés par Rosetta Relax sont présents dans le même répertoire que le script `DSSP_sur_structure_selectionnee.py`.

## Exécution de FOLDX sur les structures relaxées avec Rosetta

Après avoir préparé les fichiers avec Rosetta Relax, vous pouvez utiliser le script `auto_FOLDX_sur_ROSETTA.py` pour effectuer des analyses avec FOLDX.

### Exécution du script

1. Assurez-vous d'avoir spécifié le chemin complet vers l'exécutable FoldX dans le script.
```bash
# Spécifier le chemin complet vers l'exécutable FoldX
foldx_executable = "chemin_vers_l'foldx_executable"
```

2. Exécutez le script Python en utilisant la commande suivante dans le terminal :

```bash
python auto_FOLDX_sur_ROSETTA.py
```

Ce script effectue les opérations suivantes :

- Pour chaque fichier `_0001.pdb` généré par Rosetta Relax dans le répertoire courant :
  - Exécute l'analyse FoldX pour les chaînes A et B.
  - Supprime les fichiers `.fxout` qui ne commencent pas par 'Interface_Residues'.

Une fois le script terminé, vous aurez les résultats de l'analyse FoldX dans les fichiers `.fxout`.

## Modification du format des fichiers FOLDX pour compatibilité avec DSSP

Pour rendre les fichiers FOLDX compatibles avec les résultats de DSSP afin de permettre le croisement des informations, vous pouvez utiliser le script `FOLDX_modif_format.py`.

### Exécution du script

1. Assurez-vous que les fichiers FOLDX à modifier sont présents dans le même répertoire que le script.

2. Exécutez le script Python en utilisant la commande suivante dans le terminal :

```bash
python FOLDX_modif_format.py
```

Ce script effectue les opérations suivantes :

- Parcourt tous les fichiers `.fxout` dans le répertoire courant.
- Pour chaque fichier, il extrait les informations sur les résidus à l'interface.
- Il met à jour le format du fichier `.fxout` pour rendre les informations compatibles avec les résultats de DSSP.
- Nettoie le fichier `.fxout` en supprimant les parties indésirables.

Une fois le script terminé, les fichiers `.fxout` seront modifiés et prêts à être utilisés pour le croisement des informations avec les résultats de DSSP.

## Croisement des informations entre les fichiers DSSP et FOLDX

Pour croiser les informations entre les fichiers DSSP et FOLDX afin de regrouper les données pertinentes, vous pouvez utiliser le script `linking.py`.

### Exécution du script

1. Assurez-vous d'avoir spécifié le fichier contenant les résultats de DSSP et le fichier de sortie dans les arguments de la ligne de commande.

2. Exécutez le script Python en utilisant la commande suivante dans le terminal :

```bash
python linking.py fichier_dssp fichier_sortie
```

Remplacez `fichier_dssp` par le chemin du fichier DSSP et `fichier_sortie` par le chemin du fichier de sortie où vous souhaitez enregistrer les résultats.

Ce script effectue les opérations suivantes :

- Il lit les données à partir du fichier DSSP et du répertoire contenant les fichiers FOLDX.
- Il trouve les correspondances entre les résidus des fichiers DSSP et FOLDX basées sur l'identifiant PDB, la chaîne et la position.
- Il enregistre les correspondances dans le fichier de sortie spécifié.

Une fois le script terminé, les résultats seront ajoutés au fichier de sortie spécifié.

### Remarque

Assurez-vous que les fichiers DSSP et les fichiers FOLDX nécessaires sont accessibles et spécifiez les chemins corrects dans les arguments de la ligne de commande lors de l'exécution du script.

## Fréquence générale des résidus dans les interfaces avec une structure secondaire en feuillet β

Pour obtenir la fréquence générale des résidus se trouvant dans les interfaces et ayant une structure secondaire en feuillet β pour tous les fichiers PDB présents dans le répertoire, vous pouvez utiliser le script `freq_general.py`.

### Exécution du script

1. Assurez-vous d'avoir spécifié le fichier des résultats de `linking.py`en argument de la ligne de commande.

2. Exécutez le script Python en utilisant la commande suivante dans le terminal :

```bash
python freq_general.py fichier_de_resultats
```
Remplacez fichier1, fichier2, etc. par les noms des fichiers contenant les résultats à analyser.

Ce script effectue les opérations suivantes :

    Il lit les données à partir du fichier de résultats spécifié.
    Il génère un graphique montrant la fréquence de tous les acides aminés présents dans les interfaces avec une structure secondaire en feuillet β.
    Il sauvegarde les données dans un fichier CSV.

Une fois le script terminé, les résultats seront enregistrés dans un fichier CSV et un graphique sera généré pour chaque fichier de résultats spécifié.

## Fréquences des résidus dans les interfaces pour chaque identifiant PDB

Pour obtenir les fréquences des résidus dans les interfaces pour chaque identifiant PDB, vous pouvez utiliser le script `freq_separe.py`.

### Exécution du script

1. Assurez-vous d'avoir spécifié le fichier des résultats en argument de la ligne de commande.

2. Exécutez le script Python en utilisant la commande suivante dans le terminal :

```bash
python freq_separe.py nom_fichier_entree
```

Remplacez `nom_fichier_entree` par le nom du fichier contenant les résultats à analyser.

Ce script effectue les opérations suivantes :

- Il lit les données à partir du fichier de résultats spécifié.
- Il génère des graphiques de fréquence des acides aminés pour chaque identifiant PDB, montrant la distribution des acides aminés dans les interfaces.
- Chaque graphique est sauvegardé dans un fichier avec le nom de l'identifiant PDB suivi de `_graphique.png`.

Une fois le script terminé, vous trouverez les graphiques sauvegardés dans le répertoire de travail.
